def is_triangle(a,b,c):
    if (type(a) == int or type(a) == float) and (type(b) == int or type(b) == float) and (type(c) == int or type(c) == float) and a>0 and b>0 and c>0:
        if a+b>c:
            return True
        elif a+c>b:
            return True
        elif b+c>a:
            return True
        else:
            return False
    else:
        return "Invalid input: some of the variables are either non-positive or not numbers"